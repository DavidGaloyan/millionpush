$(document).ready(function () {
    $('#time_select').val(getFromLoc("selectedTime"));
    $('#makersfile').val(getFromLoc("MakersFileDomain"));
    $('#makerstry').val(getFromLoc("MakersTryDomain"));

    $('#time_select').on('change', function () {
        chrome.runtime.sendMessage({'selectedTime': $(this).val()}, function (response) {
            if (response.ok) {
                $('.info-alert').remove();
                $('.container').append("<div class='col-xs-12 info-alert'><label></label><div class='alert alert-info'>Interval changed!</div></div>");
            }
        });
    });

    $('.domain-btn').on('click', function () {
        if ($(this).find('button').val() == 0) {
            $('.domains').attr('disabled', true);
            $('.cancel').hide();
            $('.domain-btn').find('button').text('Change Domain').val(0).removeClass('btn-success').addClass('btn-primary');
            $('.info-alert').remove();
            $(this).next('label').show();
            $(this).parent().find('input').attr('disabled', false);
            $(this).find('button').text('Confirm').val(1).removeClass('btn-primary').addClass('btn-success');
            savedDomain =  $(this).parent().find('input').val();
            savedTryDomain = $('#makerstry').val();
            savedFileDomain = $('#makersfile').val();
        } else {
            var inputID =  $(this).parent().find('input').attr('id');
            $(this).next('label').hide();
            $(this).parent().find('input').attr('disabled', true);
            $(this).find('button').text('Change Domain').val(0).removeClass('btn-success').addClass('btn-primary');
            chrome.runtime.sendMessage({'changedDomain': $('#'+inputID).val(), 'inputID': inputID}, function (response) {
                if (response.ok) {
                    $('.info-alert').remove();
                    $('.container').append("<div class='col-xs-12 info-alert'><label></label><div class='alert alert-info'>Domain changed!</div></div>");
                }
            });
        }

    });

    $('.cancel').click(function(){
        $(this).parent().find('input').attr('disabled', true).val(savedDomain);
        if($('#makerstry').parent().find('.domain-btn').find('button').text() === 'Confirm'){
            $('#makerstry').val(savedTryDomain);
        }
        if($('#makersfile').parent().find('.domain-btn').find('button').text() === 'Confirm'){
            $('#makersfile').val(savedFileDomain);
        }

        $(this).parent().find('.domain-btn').find('button').text('Change Domain').val(0).removeClass('btn-success').addClass('btn-primary');;
        $(this).hide();
    });

    $('#makersfile').on('keyup', function(){
        if($(this).attr('disabled', false)){
            $('#makerstry').val('www.'+$(this).val()).attr('disabled', false);
            $('#makerstry').parent().
            find('.domain-btn button').
            text('Confirm').
            removeClass('btn-primary').
            addClass('btn-success').
            val(1);
            $('#makerstry').parent().find('.cancel').show();
        }
    });

    /*$('#makerstry').on('keyup', function(){
        if($(this).attr('disabled', false)){
            $('#makersfile').val('www.'+$(this).val()).attr('disabled', false);
            $('#makersfile').parent().
            find('.domain-btn button').
            text('Confirm').
            removeClass('btn-primary').
            addClass('btn-success').
            val('1');
            $('#makersfile').parent().
            find('.cancel').show();
        }
    });*/

    function getFromLoc(key) {
        var retrieved = localStorage.getItem(key);
        return JSON.parse(retrieved);
    }

});
