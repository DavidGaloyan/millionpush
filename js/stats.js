$(document).ready(function () {

    if (!getFromLoc('selectedTime')) {
        setToLoc('selectedTime', 6);
    }

    var timer = setInterval(ajaxCall, 1000 *  parseInt(getFromLoc('selectedTime')));

    function intervals() {
        clearInterval(timer);
        timer = setInterval(ajaxCall, 1000 *  parseInt(getFromLoc('selectedTime')));
    }

    function setToLoc(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    }

    function getFromLoc(key) {
        var retrieved = localStorage.getItem(key);
        return JSON.parse(retrieved);
    }

    function banned(arr) {
        return new Set(arr).size === 1;
    }

    function notificationSound() {
        var notSound = new Audio('sounds/warning.mp3');
        notSound.play();
    }

    function ajaxCall() {

        chrome.notifications.getAll(function(callback){
            $.each(callback, function(key, value){
                chrome.notifications.clear(key, function(res){
                    console.log(res);
                });
            });
            console.log(callback);
        });

        var checkLeadsFile = [];
        var checkLeadsTry = [];
        var currentdate = new Date();
        var datetime = "Last Sync: " + currentdate.getDate() + "/"
            + (currentdate.getMonth()+1)  + "/"
            + currentdate.getFullYear() + " @ "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();
        $.ajax({
            url: "http://makersfile.com/index.php?page=Offers&api_key=10000010a0e2b439ce5dc28ed8a82aa886ade42",
            type: "POST",
            success: function (response) {
                var resp = $.parseJSON(response);

                $.each(resp, function (key, value) {
                    if (value.name.indexOf('Extention') >= 0) {
                        var offerLead = parseInt(getFromLoc(value.id+'file'));
                        if (offerLead == value.leads) {
                            checkLeadsFile.push(0);
                        } else {
                            checkLeadsFile.push(1);
                        }
                        setToLoc(value.id +'file', value.leads);
                    }
                });
                console.log(banned(checkLeadsFile));
                $.each(checkLeadsFile, function (key, value) {
                    if (value === 0 && banned(checkLeadsFile)) {
                        notificationSound();
                        chrome.notifications.create(
                            {
                                type: 'basic',
                                iconUrl: "icon/cartman.png",
                                title: "Makerfile is Blocked!!!",
                                message: 'Do something!  '+datetime,
                                requireInteraction: true
                            },
                            function () {

                            }
                        );
                        return false;
                    }
                });
            }
        });

        $.ajax({
            url: "http://makerstry.com/index.php?page=Offers&api_key=1000001b1e31d8632c41b85bab77c7fe057f571",
            type: "POST",
            success: function (response) {
                var resp = $.parseJSON(response);


                $.each(resp, function (key, value) {
                    if (value.name.indexOf('Extention') >= 0) {
                        var offerLead = parseInt(getFromLoc(value.id+'try'));
                        if (offerLead == value.leads) {
                            checkLeadsTry.push(0);
                        } else {
                            checkLeadsTry.push(1);
                        }
                        setToLoc(value.id +'try', value.leads);
                    }
                });
                console.log(banned(checkLeadsTry));
                $.each(checkLeadsTry, function (key, value) {
                    if (value === 0 && banned(checkLeadsTry)) {
                        notificationSound();
                        chrome.notifications.create(
                            {
                                type: 'basic',
                                iconUrl: "icon/sindy.png",
                                title: "MakerTry is blocked!!!",
                                message: 'Do something!  '+datetime,
                                requireInteraction: true
                            },
                            function () {

                            }
                        );
                        return false;
                    }
                });
            }
        });


    }

    chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {
            if (request.selectedTime) {
                setToLoc('selectedTime', request.selectedTime);
                console.log('time '+request.selectedTime);
                onClickAjaxCall();
                intervals();
            } else if (request.changedDomain) {
                if (request.inputID === 'makersfile') {
                    setToLoc('MakersFileDomain', request.changedDomain);
                    changeUrl('makersfile.com', '10000010a0e2b439ce5dc28ed8a82aa886ade42', request.changedDomain);
                } else if (request.inputID === 'makerstry') {
                    setToLoc('MakersTryDomain', request.changedDomain);
                    changeUrl('makerstry.com', '1000001b1e31d8632c41b85bab77c7fe057f571', request.changedDomain);
                }

            }
            sendResponse({ok: true});
        });

    function changeUrl(binomUrl, apikey, domain) {
        $.ajax({
            url: "http://" + binomUrl + "/index.php?page=Offers&api_key=" + apikey,
            type: "POST",
            success: function (response) {
                var resp = $.parseJSON(response);
                var offerInfo = {};
                var original = {};

                $.each(resp, function (key, value) {
                    if (value.name.indexOf('Extention') >= 0) {
                        var parser = new URL(value.url);
                        offerInfo[value.id] = value.url.replace(parser.hostname, domain);
                        original[value.id] = value.url;
                    }
                });
                /*$.each(offerInfo, function (key, value) {
                    $.ajax({
                        type: "post",
                        url: "http://" + binomUrl,
                        data: {
                            "api_key": apikey,
                            "action": "offer_edit",
                            "id": key,
                            "url": value
                        }
                    });
                });*/
                console.log(offerInfo);
                console.log(original);
            }
        });
    }

    function onClickAjaxCall() {
        setLeadsToLoc("http://makersfile.com/index.php?page=Offers&api_key=10000010a0e2b439ce5dc28ed8a82aa886ade42", 'file');
        setLeadsToLoc("http://makerstry.com/index.php?page=Offers&api_key=1000001b1e31d8632c41b85bab77c7fe057f571", 'try');

        console.log('Request was sent!');
    }

    function setLeadsToLoc(url, index)
    {
        $.ajax({
            url: url,
            type: "POST",
            success: function (response) {
                var resp = $.parseJSON(response);
                $.each(resp, function (key, value) {
                    if (value.name.indexOf('Extention') >= 0) {
                        setToLoc(value.id+index, value.leads);
                    }
                });
            }
        });
    }


});